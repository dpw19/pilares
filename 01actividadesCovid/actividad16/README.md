# Actividad

Convocatoria de taller

# Descripción de la actividad

De manera individual deberán elaborar un cartel,
banner y/o folleto en donde se haga promoción y
difusión de su taller y las clases virtuales.

Especificando los siguientes puntos.

1. Nombre del taller

2. Nombre del tallerista

3. Horario y días de sesiones

4. Plataforma por la cual los usuarios se pueden conectar a actividades virtuales

5. Elementos visuales para atraer a posibles usuarios.

6. No utilizar logos de PILARES ni del Gobierno de la Ciudad de México

7. Cuidar que las imágenes presentadas no tengan derechos de autor.

Formato de máximo 2 hojas. 

# Fecha

13 al 17 de julio 
