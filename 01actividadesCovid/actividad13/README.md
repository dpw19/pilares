
# ACTIVIDAD

Experiencia
	
# DESCRIPCIÓN DE LA ACTIVIDAD

De manera individual deberán elaborar una presentación de su trabajo, experiencia y/o productos elaborados, relacionado con sus funciones dentro del programa "Educación para la Autonomía Económica en PILARES, 2020" es necesario incluir imágenes y su respectiva descripción. 

Presentación en formato libre de 5 cuartillas.

# FECHA

22 al 26 de junio
