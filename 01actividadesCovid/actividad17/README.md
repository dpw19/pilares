# Actividad
Planeación

# Descripción de la actividad

De manera individual deberán desarrollar las planeaciones de las próximas
tres sesiones virtuales correspondientes a su taller, considerando los 
siguientes elementos:

* Inicio
* Desarrollo
* Cierre

Con actividades, recursos y tiempos a utilizar.

Formato de Planeación.

# Fecha
20 al 24 de julio

---------------------

# Actividad

Registro

# Descripción de la actividad

Vaciar información en el formato desarrollado por Autonomía Económica con los
datos de registro.

Formato de Registro

# Fecha

20 al 24 de julio

-------------------------

# Actividad

Registro Fotográfico

# Descripción de la actividad

Llenado de formato de registro fotográfico de las sesiones virtuales que
cada uno de los talleristas está implementando, enviado por Autonomía Económica.

Formato de Registro Fotográfico.

# Fecha

20 al 24 de julio

