% Plan de trabajo Electrónica

**PILARES:** Agrícola Pantitlán   
**Taller a impartir:** Electrónica   
**Nombre del Beneficiario:** Pavel Ernesto Vázquez Martínez   
**Mes :** Abril y Mayo

 
## Módulo 1. Conceptos básicos de electrónica

### Contenido y/o tema
1.1. Magnitudes eléctricas

### Actividades a desarrollar
* Presentación y encuadre

### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón

### Actividades a desarrollar
* Exponer los conceptos básicos de la electrónica
* Carga
* Voltaje y diferencia de potencial
* Corriente eléctrica
* Resistencia y Ley de ohm
* Notación científica y prefijos m, u, n, K, M, G
	
### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón

## Módulo 1. Conceptos básicos de electrónica

### Contenido y/o tema
1.2. Componentes electrónicos

### Actividades a desarrollar
* Conocer los componentes pasivos y activos
* Conocer al Resistor, el capacitor y el inductor
* Conocer el diodo
* Conocer los transistor y sus encapsulados
* Conocer los circuitos integrados
* Arreglos básicos de transistores Darlington y Sziklai

### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón

## Módulo 1. Conceptos básicos de electrónica

### Contenido y/o tema
1.3 Simbología

### Actividades a desarrollar
* Conocer la simbología del resistor, capacitor e inductor
* Conocer la simbología del diodo
* Conocer la simbología del transistor
* Conocer la simbología de algunos circuitos integrados
* Conocer la simbología de arreglos básicos de transistores Darlington y Sziklai

### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón
* Mesa de trabajo
* Hojas de colores
* Plumones de punta fina de diferentes colores 

## Módulo 1. Conceptos básicos de electrónica

### Contenido y/o tema
1.4. Seguridad e higiene en la electrónica

### Actividades a desarrollar
* Exposición sobre medidas básicas de seguridad
* Análisis de propuestas para ordenar la mesa de trabajo
* Acuerdos para mantener un protocolo de seguridad
	
### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón
* Mesa de trabajo
* Hojas de colores
* Plumones de punta fina de diferentes colores

## Módulo 1. Conceptos básicos de electrónica

### Contenido y/o tema
1.5. Uso adecuado de herramientas y material

### Actividades a desarrollar
* Exposición sobre el uso adecuado de materiales y herramientas

### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón

## Módulo 1. Conceptos básicos de electrónica

### Contenido y/o tema
1.6.Riesgos y peligros

### Actividades a desarrollar
* Exposición sobre los riesgos en electrónica
* Exposición sobre prevención de accidentes
* Quemaduras y choque eléctrico

### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón

## Módulo 2. Instrumentación y herramientas

### Contenido y/o tema
Herramientas básicas

### Actividades a desarrollar
* Exposición sobre herramientas básicas
* Teoría y práctica sobre manejo de pinzas de punta y pinzas de corte

### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón
* Juegos de pinzas de punta y pinzas de corte para electrónica	

## Módulo 2. Instrumentación y herramientas

### Contenido y/o tema
2.2. Multímetro

### Actividades a desarrollar
* Teoría y práctica sobre el uso del multímetro
* Medición de resistencia
* Medición de voltajes y corrientes directas
* Medición de voltajes y corrientes alternas
* Prueba de diodos y continuidad
* Identificación de emisor, base y colector de transistores NPN y PNP
* Medición de temperatura
* Medición de frecuencias
	 
### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón
* Mesa de trabajo
* Multímetro digital
* Resistores de diversos valores
* Diodos rectificadores
* Diodo emisor de luz de diferentes colores
* Fuente de alimentación con voltaje variable
* Generador de funciones u oscilador
* Extensión de uso rudo

## Módulo 2. Instrumentación y herramientas

### Contenido y/o tema
2.3. Soldadura


### Actividades a desarrollar
* Exposición sobre técnica de soldadura con estación de soldar regulable y cautín tipo lápiz
* Práctica de soldadura
* Exposición sobre soldadura por ola
* Exposición sobre soldadura por ola selectiva
* Exposición sobre soldadura en pasta con horno
* Exposición sobre soldadura en pasta con pistola de calor
* Exposición sobre soldadura con bandeja o batea
* Diseño de extractor para mesa de trabajo

### Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón
* Mesa de trabajo
* Multímetro digital
* Leds de colores
* Transistores BC547 o equivalentes
* Capacitores
* Placa de PCB perforada
* Resistores de diversos valores
* Fuente de alimentación con voltaje variable
* Extensión de uso rudo
* Soldadura 60/40 de 1 mm. con núcleo de resina
* Estación para soldar con cautín tipo lápiz de temperatura regulable.
* Pinzas de punta
* Pinzas de corte
* Extensión de uso rudo 
	

## Módulo 2. Instrumentación y herramientas

### 2.4. Contenido y/o tema
Osciloscopio y generador de funciones.

### Actividades a desarrollar
* Conocer qué es un generador de funciones y qué características tiene
* Práctica con generador de funciones u oscilador
* Conocer qué es un osciloscopio y qué características tiene
* Conocer qué tipos de osciloscopio existen
* Práctica con osciloscopio digital y/o simulador 

### Material Utilizado
* Material Utilizado
* Aula de trabajo con pizarrón blanco
* Plumones para pizarrón
* Mesa de trabajo
* Multímetro digital
* Osciloscopio digital
* Generador de funciones u oscilador
* Fuente de alimentación con voltaje variable
* Extensión de uso rudo
* Pinzas de punta
* Pinzas de corte
* Extensión de uso rudo

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 3
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado

%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 3
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado

%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 3
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado


%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 4 Fuentes de tensión y corriente
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado

%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 4 Fuentes de tensión y corriente
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado

%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 4 Fuentes de tensión y corriente
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado

%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 4 Fuentes de tensión y corriente
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado


%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 5 Detección de fallas
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado

%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 5 Detección de fallas
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado

%%%%%%%%%%%%%%%%%%%%%%%%%%
Módulo 5 Detección de fallas
Circuitos

Contenido y/o tema

Actividades a desarrollar

Material Utilizado
