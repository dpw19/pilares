# Actividad

Material didáctico

# Descripción de la actividad

De manera individual deberán generar material 
funcional para la impartición de las clases en modalidad 
virtual, considerando uno de los tres estilos de
aprendizaje (auditivo, visual o kinestésico).

En caso de ser material audiovisual:
* No debe durar más de 3 minutos.
* Se debe mostrar la técnica a desarrollar, definir con claridad el 
proceso de la misma o bien mostrar el contenido claro y preciso para los
contenidos teóricos (formación y capacitación).
* Cuidar el diseño.
* No presentar imágenes o música con derechos
de autor.

# Fecha
28 de septiembre al 02 de octubre.


# Entregable

Material
