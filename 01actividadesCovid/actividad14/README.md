
# Actividad

FODA

# Descripcion de la actividad

De manera individual deberán elaborar análisis FODA,
en donde se expresen todos los puntos relevantes de 
sus funciones como Tallerista de Autonomía Económica.

Es importante considerar que: Fortaleza, Oportunidades, 
Debilidades y Amenazas conjuntan este análisis.

Entregable: Formato FODA de una cuartilla 

# Fecha

29 Junio al 3 e julio

# Actividad

Usiarios

# Descripción de la actividad

Identificar el número de usuarios por cada uno de 
los talleres que oferta Educación para la Autonomía
Económica.

Elaborar un listado con el nombre de cada usuario e 
identificar sí son continuidad; es decir, tomaron 
algún taller en el 2019.

Nombre completo,
(iniciando por apellidos)

Continuidad
(Si/No)

# Fecha

29 Junio al 3 de julio
