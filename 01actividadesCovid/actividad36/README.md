# Actividad

Trabajo comunitario

# Descripcion de la actividad

 De manera individual deberán redactar el interés o experiencia que cada uno
 tiene con respecto al trabajo comunitario, considerando los siguientes puntos:

1. No anotar la fecha de elaboración de dicho escrito.
2. No mencionar la experiencia del trabajo en PILARES 2020.
3. No incluir logos del Gobierno de la CDMX, SECTEI o PILARES.
4. Incluir en el encabezado su nombre completo (sin folio)

# Entregable

Documento PDF con la actividad solicitada

---
 
# Actividad
Clases en modalidad virtual

# Descripcion Actividad

Todos los Talleristas de Autonomía Económica deberán impartir clases en
modalidad virtual, basado en el módulo correspondiente al proceso formativo
de los usuarios.

--- 

# Actividad
Material para plataforma

# Actividad

De manera individual deberán elaborar un recurso audiovisual funcional
para la impartición de las clases en modalidad virtual.

# Entregable

Recurso Virtual
