
# Actividad
Autonomía económica
	
# Descripcion de la actividad
De manera individual deberán desarrollar o generar material para plataforma  virtual.
	
# Fecha de entrega
01 al 05 de junio.
	
# Entregable
Video

## Indicaciones
1. El vídeo debe durar de 2 a 3 minutos. 

2. El único rostro, que puede salir en el vídeo, si se requiere, (SI NO, NO HAY PROBLEMA), es el del tallerista. (NO MOSTRAR NIÑOS, NI OTRO ROSTRO AJENO AL TALLERISTA).

3. Deben dirigirse al público de manera respetuosa y sin groserías. 

4. Deben ser claros y precisos ya que la finalidad es que el usuario pueda replicarlo. 

5. Utilizar material fácil de usar o que se pueda encontrar en casa, nada complejo.
