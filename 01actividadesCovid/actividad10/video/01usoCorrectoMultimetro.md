# Sede
Pilares Agrícola Pantitlán
# Taller
Eletrónica y electrónica digital
# Gión y realización
Pavel E. Vázquez Martínez
# Licencia
Se permite el uso y adaptación de la obra
bajo la licencia Ceative Commons

Atribución: Debes dar crédito sobre la autoría de la obra
No Comercial: Evita el uso comercial de la obra
Compartir Igual: Debes compartir tus contribuciones con 
la misma licencia

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

NonCommercial — You may not use the material for commercial purposes.

ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original. 



# Uso correcto del Multímetro

## Qué es un multímetro
El multímetro es un instrumento que permite realizar la medición
de diversas variables.

Los modelos más básicos pueden medir 
* voltaje, 
* corriente,
* resistencia, 

Los más avanzados pueden medir, además,
* frecuencia
* temperatura,
* capacitancia
* inductancia

entre otras

## Cuáles son sus partes
Indicador de la medición, este puede ser analógico o digital
Un selector de la variable a medir
Al menos dos bornes para conectar las puntas de prueba
Un botón de encendido

## Cómo se utiliza el multímetro
Primero
   Elegir la variable a medir
Segundo
   Elegir una escala adecuada de medición
Tercero
   Colocar las puntas de medición en los bornes
Cuarto
   Colocar las puntas en el lugar donde se quiere conocer el valor de la variable
Quinto
   Leer el valor de la medición
