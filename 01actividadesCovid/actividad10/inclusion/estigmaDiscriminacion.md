% Estigma y discriminación
% Recopilación de información: Pavel Ernesto Vázquez Martínez
% 04/06/20

## Estigma
En Grecia se le llamaba *estigma* a las marcas en el cuerpo de los esclavos.

Cuando se le asigna una marca una persona o grupo de personas,
es decir se les estigmatiza, se les incluye en un grupo social cuyos
 miembros son vistos como inferiores o inaceptables, al grado que las
 personas que son estigmatizadas pueden llegar a asumir esa etiqueta.

## Prejuicios y estereotipos
También existen los *prejuicios* que son creencias aprendidas y juicios que se 
forman sin ningún sustento real. Estos, aunados a los *estereotipos* que son 
también creencias valores y juicios basados en información incompleta, son 
las herramientas utilizadas para justificar la discriminación de personas y
grupos de personas.

## ¿Cómo funciona el estigma?
Para que el estigma funcione se impone siempre sobre poblaciones
que aparentemente son minoritarias y que se pueden identificar por su piel,
por su idioma, por su preferencia erótico afectiva, etc.

Podemos decir que una persona que es estigmatizada
posee un rasgo que se impone a nuestra atención y que nos aleja de ella 
anulando sus restantes atributos, es decir solo se observa el estigma que le fue
impuesto.

## La sexualidad humana es más diversa de lo que se cree
En la segunda mitad del siglo pasado, a través de 5000, entrevistas se 
concluyó que la masturbación, las relaciones extramaritales y la 
homosexualidad son mucho más usuales de lo que la sociedad suponía.

## ¿Cómo se relacionan el estigma y la discriminación?
La discriminación es una práctica que niega, excluye, impide o restringe el
acceso a los derechos humanos que tienen todas las personas en condición de 
igualdad. Esta práctica se basa en las ideas generadas por los prejuicios,
los estereotipos y en los estigmas asignados a las personas o grupos de 
personas específicos.

## Diversidad y derechos
Después de largas batallas las personas de la diversidad sexual pueden 
gozar de sus derechos como seres humanos, sin embargo en mas de 70 países 
se criminalizan las relaciones consensuadas del mismo sexo entre adultos
con castigos que van desde latigazos hasta la pena de muerte.

Incluso en países donde la ley reconoce y protege los derechos humanos de
las personas integrantes de la diversidad sexual y sus aliadas, estas son
frecuentemente atacadas, se violan sus derechos e incluso llegan a ser
asesinadas.

## Glosario

* **Estigma**: Es la desacreditación culturalmente establecida que se considera
negativa hacia una persona o grupo de personas por sus características físicas
o simbólicas
* **Discriminación**: Es una práctica que niega, excluye, impide o restringe el
acceso a los derechos humanos que tienen todas las personas en condición de 
igualdad.
* **Estereotipo**: Creencias valores, juicios y suposiciones tanto positivas
como negativas, asignadas a un grupo basadas en información incompleta que
generaliza las características de algunos individuos.
* **Prejuicio**: Creencias aprendidas y juicios previos de valor positivo y
negativo que se formulan sin ningún sustento real. Pueden ir dirigidas a un
solo sujeto o a grupos. No necesariamente compartidas por la sociedad.  

## Bibliografia

Todos consultados el día 04/06/20

[Prejuicios estereotipos y estigmas](https://view.genial.ly/58b5bc00518eea0438b0166a/interactive-content-prejuicios-estereotipos-y-estigmas#:~:text=Prejuicios%2C%20estereotipos%20y%20estigmas%20by%20gustavomart25%20on%20Genially&text=Es%20la%20desacreditaci%C3%B3n%20culturalmente%20establecida,sus%20caracter%C3%ADsticas%20f%C3%ADsicas%20o%20simb%C3%B3licas.&text=Fuente%3A%20Curso%20Conapred.,Igualdad%20y%20la%20No%20Discriminaci%C3%B3n)

[Qué es la discriminación](http://data.copred.cdmx.gob.mx/comunicacion-social-y-prensa/campanas-de-difusion/campanas-2016/que-es-la-discriminacion/)

[Cartilla diversidad sexual](http://appweb.cndh.org.mx/biblioteca/archivos/pdfs/36-Cartilla-Diversidad-sexual-dh.pdf)
