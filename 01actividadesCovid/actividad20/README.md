# Actividad

Registro de usuarios

# Descripción de la Actividad

Vaciar información en el formato desarrollado por Autonomía Económica con los 
datos de registro, considerando los cambios que se han generado en la semana.
	
# Entregable

Formato de registro de usuarios.

--- 

# Actividad

Registro de asesorías

# Descripción de la Actividad
	
Vaciar debidamente la información en el formato desarrollado por Autonomía 
Económica con los datos de asesorías en caso de estar impartiendo.

Este formato será llenado sólo por aquellos talleristas que estén realizando asesorías.

# Entregable

Formato de asesorías

--- 

# Actividad

Fotografías

# Descripción de la Actividad

Llenar de manera adecuada el formato correspondiente a la evidencia fotográfica, 
considerando las características descritas en el mismo.
	
# Entregable

Formato de evidencia                       fotográfica.

--- 

# Actividad

Clases

# Descripción de la Actividad

Desarrollo de clases en modalidad virtual del taller correspondiente, con el uso de 
alguna plataforma viable para la impartición de estas.
	
# Entregable

--- 
 
# Actividad

Material didáctico

# Descripción de la Actividad

De manera individual deberán generar material funcional para la impartición de las 
clases en modalidad virtual, considerando uno de los tres estilos de aprendizaje 
(auditivo, visual o kinestésico)
	
# Entregable

Material

---  

