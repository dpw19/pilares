# ACTIVIDAD

Registro de usuarios

# DESCRIPCIÓN DE LA ACTIVIDAD

Vaciar información en el formato desarrollado por Autonomía Económica con los datos de registro.

# ENTREGABLE

Formato de registro de usuarios.

---

# ACTIVIDAD

Registro de asesorías

# DESCRIPCIÓN DE LA ACTIVIDAD

Vaciar información en el formato desarrollado por Autonomía Económica con los datos de asesorías en caso de estar impartiendo.

# ENTREGABLE

Formato de asesorías
---

# ACTIVIDAD

Fotografías

# DESCRIPCIÓN DE LA ACTIVIDAD

Llenar el formato correspondiente a la evidencia fotográfica, considerando las características descritas en el mismo.

# ENTREGABLE

Formato de evidencia fotográfica.

---

# ACTIVIDAD

Clases

# DESCRIPCIÓN DE LA ACTIVIDAD

En un mapa mental deberán esquematizar el proceso de una clase virtual, considerando desde la bienvenida hasta la conclusión de alguna actividad de reforzamiento.

# ENTREGABLE

Mapa mental.
