
# Actividad
Red

#	Descripción de la actividad

De manera individual deberán elaborar una red semántica 
con las siguientes palabras:

* Autonomía económica
* Hogar
* Emprendimiento
* Desarrollo
* Aprendizaje
* Hombre
* Cooperativismo
* Igualdad
* Empoderamiento
* Metacognición
* Mujer
* Planeación
* Técnicas
* Comunidad
* Ingresos
* Enseñanza
* Conocimiento
* Economía
* Empleo
* Compromiso

Es importante considerar que la "red semántica" es un
esquema que permite interrelacionar mediante flechas
conceptos, palabras o ideas, como su nombre lo indica
forma una Red y en ocasiones las palabras no 
necesariamente cierran un ciclo.

Máximo 2 cuartillas.

# Fecha

15 al 19 de junio
