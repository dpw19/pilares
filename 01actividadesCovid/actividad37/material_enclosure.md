% Carcasa - Enclosure - Gabinete
% [gitlab.com/dpw19/electroemprende](gitlab.com/dpw19/electroemprende)
% 10/12/2020

# Carcasa - *Enclosure* - Gabinete
Pavel E. Vázquez Mtz.

*electroemprende19@riseup.net*

---

## Carcasa, enclosure o gabinete

Los productos electrónicos que se comercian habitualmente vienen con un gabinete que brinda protección tanto a las personas que lo utilizan como al equipo en sí.

---

## Tarjetas de desarrollo

Existen algunas exepciones como las tarjetas de desarrollo.

---

### Tarjeta de desarrollo arduino

![Tarjeta de desarrolo Arduino](https://upload.wikimedia.org/wikipedia/commons/3/38/Arduino_Uno_-_R3.jpg)

---

### Tarjeta de desarrollo Raspberry Pi

![Tarjeta de desarrollo Raspberry Pi](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Raspberry_Pi_4_Model_B_-_Side.jpg/640px-Raspberry_Pi_4_Model_B_-_Side.jpg "By Miiicihiaieil Hieinizilieir / Wikimedia Commons, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=80140656")

---

## Carcasas

Existen muchos tipos de carcasas y pueden ser de pueden ser
 diferentes materiales

* Acrílico
* Fierro
* Aluminio
* Plástico

 ---

## Standar IP - NEMA

Existe un estándar llamado IP o NEMA que indica cuál es el nivel de protección de las carcasas o gabinetes

---

### IP - XX

El estándar IPXX indica el nivel de protección de acuerdo a la combinación de dos números

* El primer numero representa la protección contra cuerpos sólidos [0 - 6]
* El segundo numero representa el nivel de protección contra Líquidos [0 - 8]

---

## Diseño de Carcasas

En la actualidad podemos diseñar nuestros propios gabinetes o carcasas con programas de modelado 3d como FreeCAD

![Logo de FreeCad](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/FreeCAD-logo.svg/480px-FreeCAD-logo.svg.png)

---

## Proteccion / Producto

Con una carcasa podemos pasar del prototipo finalizado a un producto con posibilidades de mercadeo.

---

### Prototipo finalizado

Prototipo finalizado, es funcional, está probado pero es difícil de comercializar.

 ![Fuente de alimentacion sin gabinete](img/FuenteAlimentacion.jpg)


 ---

### Producto finalizado

Coando colocamos el proyecto finalizado en un gabinete entonces podemos hablar de un producto.

  ![Fuente de alimentacion sin gabinete](img/FuenteGabinete.jpg)

---

## Actividad Pros y Contras

* Realiza un listado de las ventajas que notas al colocar tus proyectos en gabinetes adecuados.

* ¿Qué desventajas notas en este proceso?
