 
# Actividad
Clases en modalidad virtual

# Descripcion Actividad

Todos los Talleristas de Autonomía Económica deberán impartir clases en
modalidad virtual, basado en el módulo correspondiente al proceso formativo
de los usuarios.

--- 

# Actividad
Material para plataforma

# Actividad

De manera individual deberán elaborar un recurso audiovisual funcional
para la impartición de las clases en modalidad virtual.

# Entregable

Recurso Virtual
