# Educación Comunitaria:


# Definición
Entre otras cosas la educación comunitaria plantea que la persona 
"aprende en la acción la cual debe estar acompañada por un conocimiento
de la realidad", formación-acción. [pp 324]

Además de que las personas "conozcan su propia existencia, sus ideales
 y sus posibilidades de acción con el resto de los sujetos que en su 
 entorno intercambian con el mundo social". [pp 326] 

# Poblaciones que atiende
A todas las poblaciones que "aprendan en la acción acompañada por
un conocimiento de la realidad" [pp 325]. 

# Importancia en población CDMX
Dado que "el ser comunitario aprenden rompiendo con las imposiciones
y por su misma condición hace valer sus propios conocimientos y
experiencias" [pp 324] Permite generar una ciudadanía consciente 
de su entorno y a su vez demandante de sus derechos.

# Correlación con PILARES
En cierto sentido PILARES puede ser un espacio que 
"impulse la reflexión sobre la vinculación
pensamiento-ser-realidad" [pp 328] en tanto la
institución encuentre el modo de acercarse y
escuchar estas reflexiones como otro ente también 
comunitario.

# Relación con EAE
Se relaciona en el sentido de que las personas "pueden
discutir sus problemas culturales, económicos, políticos
y coincidir en la búsqueda de soluciones, lo que constituye
un aprendizaje significativo" [pp 326] y que ademas 
les convierte en ejecutores del cambio en su propia realidad.



Los textos citados se obtuvieron de artículo:
"La educación comunitaria: una concepción desde la pedagogía de la 
esperanza de Paulo Fraeire" de Enrique Pérez Luna y José Sánchez 
Carreño, publicado en la Revista Venezolana de Ciencias Sociales
UNERMB, Vol. 9 No. 2, 2005
