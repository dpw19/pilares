# Actividad 9

## Actividad
Dibujando la Educación Comunitaria.

## Descripcione la actividad
De manera individual deberán elaborar un mapa mental que represente los siguientes puntos:

1. Definición de Educación Comunitaria.

2. ¿A qué población atiende la enseñanza comunitaria?

3. Importancia de la Educación Comunitaria en la población de la Ciudad de México.

4. Correlación de Educación Comunitaria con PILARES.

5. Relación que tiene la Educación Comunitaria con Autonomía Económica.

Es importante considerar que un mapa mental es un diagrama que
representa ideas, conceptos, palabras, etc. Ligado a través 
de una idea central, por lo que se deberá responder a los
puntos anteriores de esa manera.

## Fecha
25 - 29 de mayo
 
## Entregable
Mapa mental
