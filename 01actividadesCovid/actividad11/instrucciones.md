# Actividad
Autonomía económica

# Descripción de la actividad
De manera individual deberán desarrollar o generar material para plataforma virtual.

# Fecha de entrega
08 al 12 de junio.
	
# Entregable
Material didáctico; Video

# LINEAMIENTOS DEL VIDEO
1. El vídeo debe durar de 2 a 3 minutos.
2. El único rostro, que puede salir en el vídeo, si se requiere, es el del tallerista. (NO MOSTRAR NIÑOS, NI OTRO ROSTRO AJENO AL TALLERISTA).
3. Deben dirigirse al público, en la explicación del tema, de manera respetuosa y sin groserías.
4. Deben ser claros y precisos ya que la finalidad es que el usuario pueda replicarlo.
5. Utilizar material fácil de usar o que se pueda encontrar en casa, nada complejo.
6. En caso de estar muy pesado el video, se puede agregar la dirección URL (enlace de Internet) del sitio donde está almacenado, OneDrive, Drive, Dropbox, etc; es importante, no subir dicho video a las redes sociales (Facebook, Instagram, Snap chat, YouTube, etc).
7. Tener cuidado de no usar logos y melodías con derecho de autor.

# LINEAMIENTOS GENERALES.
1. Colocar en el Asunto del correo electrónico, el NOMBRE DE TU TALLER Y EL NOMBRE DE LA ACTIVIDAD.

2. En el cuerpo del correo colocarás NOMBRE COMPLETO COMENZANDO POR APELLIDO PATERNO

3. Colocar también en el cuerpo del corro la frase "SI COVID" o "NO COVID" en caso de presentar síntomas, algún familiar o algún vecino.

4. También colocar la frase "SI HERRAMIENTA" o "NO HERRAMIENTA" en el cuerpo del correo en caso de contar con algún medio de grabación (celular, Ipad, Tablets, Computadora, cámara de video u otro medio; especificar cuál.

Ejemplo: SI HERRAMIENTA: Cámara o NO HERRAMIENTA.

5. Enviar con copia al correo de tu LCPO Administrativo, así como al Subdirector Operativo correspondiente.
