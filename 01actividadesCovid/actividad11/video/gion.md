# Detención de fallas comunes en un dispositivo móvil

## Fallas del eliminador
El eliminador deja de proporcionar un voltaje adecuado a su salida

Para saber si el eliminador está generando un voltaje a la salida
hay que medirlo con un multímetro. 

## Fallas del cable usb
Cable trozado o cable no apto para la cantidad de corriente de carga

## Fallas de la batería
Se utilizan baterías de litio por su rápida carga y gran
capacidad de energía.

Son altamente contaminantes e inestables si no se utilizan correctamente.

Suelen inflarse o perder la capacidad de carga.

## Fallas del conector microusb (ogt)
El conector suele desoldarse de sus pequeños pines SMD.

Es conveniente reemplazarlo.

Requiere técnicas de soldado para componentes SMD. Suele
hacerse con una pistola de calor.

## Fallas del display
El display pierde la iluminación, alguna zona no se activa


## Fallas del touch
El touch que suele ser capacitivo se rompe a menudo o pierde la sensibilidad

## Fallas de los botones
Estos dejan de hacer contacto por el sudor, humedad que genera falsos contactos

## fallas del sensor de luz
El sensor de luz permite que la pantalla se apague cuando el 
dispositivo se acerca al rostro.

En ocasiones solamente esta sucia la carcasa y hace parecer que es  una falla de display. 


