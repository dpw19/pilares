
# Actividad

Medidas de seguridad

# Descripción de la actividad

De manera individual deberán elaborar un recurso visual 
(infografía cartel, diagrama, etc.) en donde se esquematicen 
los siguientes puntos:

1. Las medidas de seguridad e higiene que se requieren seguir
una vez que se incorporen a actividades dentro de los espacios
asignados para la impartición del taller.

2. Promoción del seguimiento de dichas medidas de seguridad e higiene.

## Entregable

Formato libre con el recurso visual 

# Fecha

6 al  10 de julio
