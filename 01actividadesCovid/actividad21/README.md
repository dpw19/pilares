# Actividad

Material didáctico

# Descripción de la actividad

De manera individual deberán generar material funcional para la impartición 
de las clases en modalidad virtual, considerando uno de los tres estilos de
 aprendizaje (auditivo, visual o kinestésico).

En caso de ser material audiovisual:

* No debe durar más de 3 minutos.
* Se debe mostrar la técnica a desarrollar, definir con claridad el 
proceso de la misma o bien mostrar el contenido claro y preciso para 
los contenidos teóricos (formación y capacitación).
* Cuidar el diseño.
* No presentar imágenes o música con derechos de autor.

# Entregable

Material

---

# Actividad

Registro de usuarios

# Descripción de la actividad

Vaciar información en el formato desarrollado por Autonomía Económica con 
los datos de registro, considerando los cambios que se han generado en
la semana.

# Entregable

Formato de registro de usuarios.

---

# Actividad

Registro de asesorías

# Descripción de la actividad

Vaciar debidamente la información en el formato desarrollado
por Autonomía Económica con los datos de asesorías en caso de
estar impartiendo.

Este formato será llenado solo por aquellos talleristas que 
estén realizando asesorías.      

# Entregable

Formato de asesorías

---

# Actividad

Fotografías

# Descripción de la actividad

Llenar de manera adecuada el formato correspondiente a la 
evidencia fotográfica, considerando las características descritas en el mismo.

# Entregable

Formato de evidencia fotográfica.


