# Actividad

Material didáctico.

# Descripción de la actividad

De manera individual deberán generar material funcional para la impartición
de las clases en modalidad virtual, considerando uno de los tres estilos 
de aprendizaje (auditivo, visual o kinestésico).

## En caso de ser material audiovisual

* No debe durar más de 3 minutos.
* Se debe mostrar la técnica a desarrollar, definir con claridad el 
proceso de esta o bien mostrar el contenido claro y preciso para los
contenidos teóricos (formación y capacitación).
* Cuidar el diseño.
* No presentar imágenes o música con derechos de autor.

## En caso de ser material visual

* Debe exponer con claridad el contenido.
* No debe contener ninguna imagen con derechos de autor.
* Cuidar el diseño
* Cuidar el formato
* Cuidar el formato de letras

## En caso de ser material kinestésico

* Cuidar la información que se está presentando.
* Asegurarse que se dan las indicaciones de seguridad.
* Considerar que se debe contener conocimiento previo para
la elaboración de estos ejercicios.

# Entregable

Material

