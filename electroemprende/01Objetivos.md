# Taller de Electroemprende

## Objetivos de Pilares


## Objetivos de la Subdirección de Autonomía Económica

La subdirección de Autonomía Económíca (AE) tiene los siguientes objetivos

### Objetivo General

Contribuir al ejercicio de los derechos a la educación, al desarrollo
sustentable, al empleo, a la cultura, al deporte, buscando mejorar la calidad
de vida de la ciudadania, para buscar así disminuir los niveles de violencia
y delincuencia en nuestra ciudad.

### Objetivo Partícular

Implementar estrategias de Educación para la Autonomía Económica (EAE), buscando
alcanzar una economía más social, sostenible, equilibrada y local que responda
a un modelo más justo de creación, mantenimiento y reparto del empleo en las
colonias, barrios y pueblos en donde se encuentren los PILARES.

### Campos de formación

Impulsamos un modelo de desarrollo con el Programa de Educación para la 
Autonomía Económica, el cual fortalecerá tres campos de formación:

1. Aprendizaje de técnicas de producción de bienes y servicios (acordes con las 
regiones urbanas y las vocaciones locales).

1. Formación para la organización productiva del empleo (Cooperativismo,
emprendedurismo, empleo y autoempleo).

1. Capacitación para comercializar, incluyendo comercio digital (Empacar, 
distribuir, brindar servicio, identidad comercial y marca, etc.)

## Objetivos del taller de electrónica

### Objetivo general (qué, cómo, para qué)

Fomentar el acceso a la Autonomía Económica
por medio de la aplicación de la electrónica
en la generación, seguimiento, desarrollo y/o implementación de productos
o servicios que generen emprendimiento o autoempleo.

### Objetivos específicos

Definición desarrollo y seguimiento de un proyecto de emprendimiento o
autoempleo.

Estudio y aplicación de conceptos de electrónica que permitan el desarrollo del 
proyecto.

## Perfil de las personas

Son bienvenidas todas las personas con
* Gusto por el trabajo colaborativo
* Disposición
* Iniciativa e inventiva
* Deseo de adentrarse al mundo de la electrónica


## Flujo de trabajo

Para lograr los objetivos se sugiere el siguiente flujo de trabajo

1. Presentación del taller y sus alcances
1. Emprendimiento.
	1. Sólo requieres una idea para empezar.
	1. Escribe tu idea y realiza una presentación.
	1. Recibe comentarios.
	1. Reajusta tu proyecto.
	1. Dibuja cómo te imaginas el proyecto final.
	1. Recibe comentarios
1. Estudia los conceptos básicos de electrónica.
1. Estudia conceptos específicos de electrónica.
1. Aplica tus nuevos conocimientos de electrónica en tu proyecto de 
emprendimiento o autoempleo.
1. Realiza prototipos.






Preparación:

Conducción:
   Encuadre desarrollo y cierre
   Técnicas instruccionales

Evaluación




Eco217
https://517de8f6-d929-493a-a543-05addfd00715.filesusr.com/ugd/76e5f8_ef798480393d4d90a474304113ce1709.pdf


Código:NUGCH002.01
Título:Diseño de cursos de capacitación presenciales, sus instrumentos de evaluación y material didáctico
https://conocer.gob.mx/RENEC/fichaEstandar.do?method=obtenerPDFEstandar&idEstandar=213


EC0049 Diseño de cursos de capacitación presenciales, sus instrumentos de evaluación y material didáctico. 

 
