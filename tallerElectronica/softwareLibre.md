% Software libre y Pilares
% Pavel E. Vázquez Mtz. [pavel\_e@riseup.net];  
  Carlos Osvaldo Cervantes Lima [cerlimcar@gmail.com]
% 04/07/2019
---

# Software Libre y Pilares

El acrónimo PILARES incluye dentro de sus letras la palabra
libertad. Si hablamos de Software (programas para computadoras) realizados
de forma ética nos encontraremos con la Filosofía del Software libre que
en la actualidad se denomina así tanto en español como en inglés para
evitar las confusiones que imprimía el término Free Software, ya que *Free*
puede traducirse como *libre* y también como *gratis*. 
La libertad planteada por el software libre va más allá de la gratuidad
y plantea cuatro libertades que los programas deben brindar a quienes 
los utilizan [1].

* **Libertad 0:** La libertad de ejecutar el programa como se desee,
con cualquier propósito
 
* **Libertad 1:** La libertad de estudiar cómo funciona el programa,
y cambiarlo para que haga lo que usted quiera. **El acceso al código 
fuente es una condición necesaria para ello.**

* **Libertad 2:** La libertad de redistribuir copias para ayudar a otros.

* **Libertad 3:** La libertad de distribuir copias de sus versiones
modificadas a terceros. Esto le permite ofrecer a toda la comunidad
la oportunidad de beneficiarse de las modificaciones. El acceso al 
código fuente es una condición necesaria para ello.
 
El proyecto GNU ha generado una licencia llamada GNUGPL (Licencia 
pública General), esta licencia garantiza que los programas 
desarrollados bajo estos términos promuevan y respetan las libertades
antes mencionadas.   

# Educación para la Autonomía Económica y Software Libre

El área de Educación para la Autonomía Económica así como las demás áreas
dentro de pilares puede aplicar
el uso de software libre para reforzar sus talleres. Aquí un listado de 
algunos programas sugeridos.


## GNU/Linux
### Descripción
Sistema operativo. Es el inicio de un verdadero cambio en torno
al uso de Software Libre.  
Existe una gran variedad de *distribuciones* GNU/Linux y es importante elegir una 
que promueva las libertades antes mencionadas.

### Tipo de licencia
En general cuentan con una licencia GNU GPL, lo que debe cuidarse es qué
licencias tienen los programas que se instalan después.

## Inkscape
### Descripción
Inkscape es un editor profesional de gráficos vectoriales.

### Tipo de licencia
GNU GPL 

### Sitio del proyecto
[https://inkscape.org/](https://inkscape.org/)

## Gimp
### Descripción
Programa para edición de imágenes en mapa de bits.  
Este programa ya está instalado en los equipos de computo de las
ciberescuelas, mismas que al momento de redactar este documento aún
utilizan un sistema operativo privativo que nulifica la libertades
que promueve el software libre.

### Tipo de licencia
GNU GPL

### Sitio del proyecto
[https://inkscape.org/](https://inkscape.org/)

## Blender
### Descripción
Programa para animación 3D.  
Es uno de los mejores programas para la edición de video, modelado 3D,
animación 3D, etc.

### Tipo de licencia
GNU GPL 

### Sitio del proyecto
[https://www.blender.org](https://www.blender.org)

## Kdenlive
### Descripción
Programa para edición de video no lineal.  
Se basa en el framework Multimedia MLT [https://www.mltframework.org/](https://www.mltframework.org/). 
Otros editores de video basado en MLT muy utilizados son

* Shotcut [https://shotcut.org/](https://shotcut.org/)
* OpenShot [https://www.openshot.org/](https://www.openshot.org/)

Entre más de una veintena de proyectos.

### Tipo de licencia
GNU GPL

### Sitio del proyecto
[https://kdenlive.org/es/](https://kdenlive.org/es/)


## Kicad
### Descripción
Suite para automatización de diseño electrónico.

Cuenta con:

* Captura de esquemáticos
* Diseño de PCB
* Visor 3D

### Tipo de licencia
GNU GPL

### Sitio del proyecto
[http://kicad-pcb.org/](http://kicad-pcb.org/)


## Arduino IDE
### Descripción
El IDE de Arduino hace sencillo escribir código y subirlo a la tarjeta
desarrollo Arduino.

### Tipo de licencia
GNU GPL

### Sitio del proyecto
[https://www.arduino.cc/en/Main/Software#source](https://www.arduino.cc/en/Main/Software)

## Atom
### Descripción
Un editor de texto hackeable para el siglo XXI

### Tipo de licencia
MIT

### Sitio del proyecto
[https://atom.io/](https://atom.io/)

## Pandoc
### Descripción
Se utiliza para convertir archivos de un formato de etiquetado a otro.

### Tipo de licencia
GNU GPL

### Sitio del proyecto
[https://pandoc.org/](https://pandoc.org/)

## VLC
### Descripción
Reproductor multimedia  famework que reproduce la mayoría de archivos
así como DVDs, audio CDs, VCDs, y varios protocolos streaming. 

### Tipo de licencia
GNU GPL

### Sitio del proyecto
[https://www.videolan.org/vlc/](https://www.videolan.org/vlc/)

