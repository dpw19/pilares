# Sobre Taller de Electrónica y Robótica

## Datos de contacto
* Pavel E. Vázquez Mtz.
* pavel_e@riseup.net
* Versión digital: https://gitlab.com/dpw19/pilares/blob/master/Sobre%20el%20taller%20de%20electr%C3%B3nica.md

## Antecedentes

El uso de la tecnología parece supeditado a la recepción y uso pasivo
de la misma. Sin embargo también en el desarrollo y uso de diversas
tecnologías podemos y debemos hablar de derechos y de ética, generar personas
conscientes, lo que se conoce en la actualidad como ciudadanía digital, a través 
de la alfabetización digital.

Desde hace más de una década se ha generado en diferentes latitudes el 
movimiento llamado Maker y la metodología de aprendizaje STEAM (Science, 
Technology, Engineering, the Arts and Mathematics) [^1]. No es casualidad que 
hasta hace muy poco tiempo en México se comience a hablar del tema y a generar 
espacios con esta perspectiva u otras como las Herratecas (lo mismo que las 
bibliotecas pero con herramientas en vez de libros).

Estos movimientos que socializan las tecnologías y los saberes son posibles
gracias a la existencia de filosofías como la del software libre impulsada por
Richard Stallman fundador de la [Free Software Foundation](https://www.fsf.org/)
que derivó en la licencia GPL que 
brinda 4 libertades fundamentales a las personas que utilizan programas, la 
cultura de generar obras artísticas bajo licencias
[Creative Commons](https://creativecommons.org/), o la creación de 
[Free Hardware](https://www.gnu.org/philosophy/free-hardware-designs.en.html) 
cuyo ejemplo más contundente son las impresoras 3D.

Bajo estos antecedentes considero importante aplicar de forma transversal estos
principios en los talleres de ámbito tecnológico.

## Algunas ideas de aplicación


Un primer paso es generar un ecosistema de software libre ya que se puede hacer
con el equipo de cómputo existente y permite conocer programas libres y aplicar
la alfabetización digital desde plataformas congruentes.
Al ser software libre no se requiere una inversión económica aunque es
recomendable hacer alguna aportación a los proyectos.
 
* Sistema pperativo GNU/Linux
* Aplicaciones con licencia de software libre
	+ Inkscape (para graficos vectoriales)
	+ Gimp (para graficos de mapa de bits)
	+ Libreoffice (Herramientas de oficina)
	+ Kicad (Diseño de PCB)
	+ Free cad (Diseño 3d)
	+ Kdenlive (Edición de video)
	+ un largo etc ...


Un segundo paso para avanzar en la perspectiva Maker es generar un ecosistema
básico de harware libre, este paso puede ser paralelo al anterior.

* Tarjetas de desarrollo arduino, raspberry, launchpad, etc.
* Shield de expansión
	+ Sensores
	+ WIFI
	+ Interfaces
	+ Actuadores
	+ otro largo etc.
* Herramientas básicas
	+ Pinzas
	+ Cautínes
	+ Fuentes de alimentación
	+ Multímetros
	+ Osciloscopio
* Conectores, cables, etc.
* Componentes básicos de electrónica
* Kit para ensamblar impresora 3D
* Insumos de impresión 3D.
* Impresora 3D
* Máquina CNC

## Público al que va dirigido

* Niñas y niños de 8 años en adelante y personas en general con interés y/o 
habilidades en temas científicos, tecnológicos, artísticos.
* Niñas y niños de 8 años en adelante y personas en general que les guste
trabajar en equipo y aprender a la vez que comparten sus conocimientos.
* Niñas y niños de 8 años en adelante y personas en general que deseen
desarrollar algún proyecto personal, escolar, etc. de forma individual o 
colectiva.

## Lineamientos de seguridad

Al ser un espacio que cuenta con equipo y herramientas de diversos tipos es
importante socializar y ejercitar su uso constantemente pa permitir el 
aprendizaje entre pares.

## Acuerdos de convivencia

Para generar un espacio incluyente y libre de violencia se sugiere implementar
estrategias basadas en la educación para la paz y la cultura del buen trato.

Los acuerdos particulares se generarán con las personas que participen en el
taller.

## Notas al pie

[^1]: Segun https://educationcloset.com/steam/what-is-steam/ 
"STEAM is an educational approach to learning that uses Science, Technology, 
Engineering, the Arts and Mathematics as access points for guiding student 
inquiry, dialogue, and critical thinking. The end results are students who
take thoughtful risks, engage in experiential learning, persist in 
problem-solving, embrace collaboration, and work through the creative process.
These are the innovators, educators, leaders, and learners of the 21st century!"